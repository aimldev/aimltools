# aimltools - generic tools for pytorch, docker etc.

## Docker container for Pytorch

## Build Image

cd docker/pytorch
sh buildDocker.sh

## Run Container 

    nvidia-docker run -it --rm -v ~/code/aimltools:/data/aiml -v /data/datasets:/data/datasets aiml-pytorch 

Test it out:

    python /data/aiml/pytorchtest.py


To get X-windows from inside the container, there are instructions here:

    http://wiki.ros.org/docker/Tutorials/GUI

Using the lazy and dangerous method, you do:

    xhost +local:root
    nvidia-docker run --ipc=host -it --rm -v ~/code/aimltools:/data/aiml -v /data/datasets:/data/datasets --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"   aiml-pytorch

## Train on xview data

The script `aimlTrainPytorch.py` is based on the Pytorch ImageNet training example.  It allows for distributed
training, and imgaug is (or will be) incorporated.  At the moment it loads standard architectures and fine
tunes them for classification.

    export CUDA_VISIBLE_DEVICES="0 1"
    cd /data/aiml
    ./aimlTrainPytorch.py --plot -a vgg16 --lr 0.001  --pretrained --workers 8  --gpu 0 /data/data_ssd/stormcloud/source/ -b 32 --imgaug --weightedSampling --savePath results/source_domain

This will train on the given data set (stormcloud source) and write model files to results/source_domain.  To
evaluate a trained model on the test set, use the "-e" flag:

    ./aimlTrainPytorch.py --plot -a vgg16 --lr 0.001  --workers 8  --gpu 0 /data/data_ssd/stormcloud/target/ -b 32 -e --resume results/source_domain/checkpoint_00000001_best.pth.tar  
