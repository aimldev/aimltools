import numpy as np

def inRange(x, xmin, xmax):
    if np.isscalar(x):
        return xmin <= x and x <= xmax
    else:
        return np.logical_and(xmin <= x, x <= xmax)
