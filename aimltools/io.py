from PIL import Image
import numpy as np


# There are many ways to read images in python.  Some give unexpected results, for example:
#   - opencv loads RGB as BGR
#   - skimage can convert to range [0,1]
#   - matplotlib can load greyscale images as 3-channel images
# Pillow seems pretty sensible and reliable, so using it here.
def readImage(fn):
    return np.array(Image.open(fn))

def makeRGB(img):
    if img.ndim == 2:
        return np.dstack([img, img, img])
    else:
        return img
