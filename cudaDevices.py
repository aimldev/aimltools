#!/usr/bin/env python3
import torch

for i in range(3):
    device = torch.cuda.device(i)
    print('%d: %s' % (i,torch.cuda.get_device_name(i)))
