#!/usr/bin/env python3

'''Uses features from a model trained with aimlTrainPytorch.py to create a t-sne embedding.
'''

import argparse

def parseArgs():
    model_names = sorted(name for name in models.__dict__
                         if name.islower() and not name.startswith("__")
                         and callable(models.__dict__[name]))

    parser = argparse.ArgumentParser(description='PyTorch ImageNet Training',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('data', metavar='DIR', help='path to dataset')

    parser = atr.addParametersDataLoader(parser)
    parser = atr.addParametersModelLoader(parser)

    parser.add_argument('--plot', action='store_true', help='Plot some stuff during training')
    parser.add_argument('--savePath', default=None, type=str, help='path to write model checkpoints')
    parser.add_argument('--interactive', action='store_true', help='Keep plots at end')
    parser.add_argument('--tsnePerplexity', type=float, default=15.0, help='Perplexity in tsne alg')
    parser.add_argument('--tsneLR', type=float, default=50.0, help='Learning rate in TSNE alg')

    return parser.parse_args()


import seaborn as sns; sns.set()
import aimlTrainPytorch as atr
from tsnecuda import TSNE
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import torchvision.models as models
import os
import torch
import colorsys

def _get_colors(num_colors):
    colors=[]
    for i in np.arange(0., 360., 360. / num_colors):
        hue = i/360.
        lightness = (50 + np.random.rand() * 10)/100.
        saturation = (90 + np.random.rand() * 10)/100.
        clr01 = colorsys.hls_to_rgb(hue, lightness, saturation)
        colors.append([z*255.0 for z in clr01])
    return colors

# colours is a list of (r,g,b) tuples on range [0,255].
# Weights in [0,1] used to dull colours.
def scatterLabel( x,
                  y,
                  labels,
                  labelNames=None,
                  alpha=0.8,
                  sz=20,
                  doLegend=False,
                  doGrid=False,
                  colours=None,
                  weights=None ):
    assert len(x) == len(y)
    if weights is None:
        weights = np.ones((len(x),), dtype=float)
    assert len(weights) == len(x)
    assert len(labels) == len(x)

    nbClasses = len(np.unique(labels))
    if colours is None:
        colours = _get_colors(nbClasses)
    #print('colours = {}'.format(colours))
    for lab in np.unique(labels):
        sel = labels == lab
        xx, yy = x[sel], y[sel]
        clrRGB = colours[lab]
        # This is rgba.  We use alpha in here; using the arg version over-rides.
        clr = np.array([1.0, 1.0, 1.0, alpha])
        clr[:3] = clrRGB
        clr[:3] /= 255.0
        clrMat = np.tile(clr, [len(xx), 1])
        # Apply weights
        theseWeights = weights[sel]
        if 0:
            # with this scheme we make them darker
            clrMat *= theseWeights[:,np.newaxis]
        else:
            # with this scheme we fade with alpha
            clrMat[:,3] *= theseWeights

        plt.scatter(xx, yy, c=clrMat, label=labelNames[lab], s=sz)
        #plt.waitforbuttonpress()
    if doGrid:
        plt.grid(1)
    if labelNames is not None and doLegend:
        plt.legend()

# Use a class for this so we only load the model once.  Also computes t-sne on an initial data set.
# It's quite unfortunate that you have to re-compute the full embedding from all data each time.
class TsneComputer:
    def __init__( self,
                  classes,
                  model=None,
                  pretrained=False,
                  arch='vgg16',
                  distributed=False,
                  gpu=None,
                  batch_size=256,
                  workers=4,
                  resumePath=None,
                  perplexity=15,
                  learningRate=10):
        self.gpu = gpu
        # Load model
        C       = len(classes)
        #print('Data set has {} classes: {}'.format(C, classes))
        if model is None:
            self.model, _ = atr.loadModel( # dummy lr since we aren't training
                C, pretrained, arch, distributed, gpu, batch_size, workers, 0.1, resumePath
            )
        else:
            self.model = model

        # Strip off classification layer so we get the last layer of features.
        cl           = self.model.classifier
        lastLayerIdx = list(cl._modules.keys())[-1]
        lastLayer    = cl._modules[lastLayerIdx]
        inSize       = lastLayer.in_features
        del self.model.classifier._modules[lastLayerIdx]

        self.perplexity = perplexity
        self.learningRate = learningRate
        self.tsne = None

    def computeFeatures(self, dataLoader):
        # Compute features of data using model
        outputs = []
        labels  = []
        self.model.eval()

        with torch.no_grad():
            for i, (input, target) in enumerate(dataLoader):
                #print('batch {}'.format(i))
                #print('target = {}'.format(target))
                if self.gpu is not None:
                    input = input.cuda(self.gpu, non_blocking=True)
                    target = target.cuda(self.gpu, non_blocking=True)

                labels.append(target.cpu().numpy())
                input = input.cuda(self.gpu, non_blocking=True)
                target = target.cuda(self.gpu, non_blocking=True)
                #print('input = {}'.format(input))
                output = self.model(input)
                #print('output  = {}'.format(output.shape))
                outputs.append(output.cpu().numpy())
                #break #!!

        X = np.vstack(outputs) #torch.cat(outputs, 0)
        #print('X shape = {}'.format(X.shape))
        y = np.concatenate(labels)
        return X, y

    def computeEmbeddingOfFeatures(self, X, labels, initEmbedding=None):
        self.tsne = TSNE( n_components=2,
                          perplexity=self.perplexity,
                          learning_rate=self.learningRate )
        Xemb = self.tsne.fit_transform(X, y=initEmbedding)
        return Xemb, labels

    # Returns data transformed to tsne output space.
    def computeEmbedding(self, dataLoader):
        # Use these to create tsne embedding
        X, labels = self.computeFeatures(dataLoader)
        Xemb, _ = self.computeEmbeddingOfFeatures(X, labels)
        return X, Xemb, labels


# eg.         classes = dataLoader.dataset.classes
def createTsneComputerArgs(args, classes):
    C       = len(classes)
    model, _ = atr.loadModelArgs(args, C)
    return TsneComputer( classes,
                             model,
                             args.pretrained,
                             args.arch,
                             args.distributed,
                             args.gpu,
                             args.batch_size,
                             args.workers,
                             args.resume,
                             args.tsnePerplexity,
                             args.tsneLR)



if __name__ == '__main__':
    plt.ion()


    if 0:
        scatterLabel(np.array([0,1,2,3,4]), np.array([2,1,3,0,5]), np.array([0, 2, 0, 1, 1]), \
                     labelNames=['cows', 'goats', 'sheep'])
        plt.ioff()
        plt.show()

    args = parseArgs()
    args.distributed = False

    if args.savePath and not os.path.isdir(args.savePath):
        print('Creating output directory {}'.format(args.savePath))
        os.mkdir(args.savePath)

    # Load data
    train_loader, val_loader, _ = atr.loadDataSetsArgs(args)

    classes = train_loader.dataset.classes
    tsc = createTsneComputerArgs(args, classes)
    X, Xemb, labs = tsc.computeEmbedding(val_loader)
    print('X embedded shape = {}'.format(Xemb.shape))

    # Save tsne model, and 2d visualisation points.
    plt.figure()
    scatterLabel(Xemb[:,0], Xemb[:,1], labs, labelNames=classes)
    plt.waitforbuttonpress(0.0001)


    if args.interactive:
        plt.ioff()
        plt.show()
