#!/usr/bin/env python3

'''This script is derived from pytorch/examples/master/imagenet/main.py
'''

import argparse
import os
import random
import shutil
import time
import warnings
import sys
import matplotlib.pyplot as plt
import numpy as np
import glob

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
import torchvision
import torchvision.utils

import focalloss
import pdb

from imgaug import augmenters as iaa
from PIL import Image

from torchsummary import summary

model_names = sorted(name for name in models.__dict__
    if name.islower() and not name.startswith("__")
    and callable(models.__dict__[name]))

CHANNEL_MEAN = np.array([0.485, 0.456, 0.406])
CHANNEL_STD  = np.array([0.229, 0.224, 0.225])


def addParametersDataLoader(parser):
    parser.add_argument('--imgaug', action='store_true', help='Do image augmentation with imgaug library')
    parser.add_argument('--imgaugClouds', action='store_true', help='Do image augmentation with clouds')
    parser.add_argument('--weightedSampling', action='store_true',
                        help='Use class count disproportionate weighted sampling during training')
    parser.add_argument('-b', '--batch-size', default=256, type=int,
                        metavar='N',
                        help='mini-batch size (default: 256), this is the total '
                             'batch size of all GPUs on the current node when '
                             'using Data Parallel or Distributed Data Parallel')
    parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                        help='number of data loading workers (default: 4)')
    return parser


def addParametersModelLoader(parser):
    parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                        help='use pre-trained model, ie load pre-trained weights into chosen architecture.  '
                        'Otherwise, random initialisation.')
    parser.add_argument('-a', '--arch', metavar='ARCH', default='resnet18',
                        choices=model_names,
                        help='model architecture: ' +
                            ' | '.join(model_names) +
                            ' (default: resnet18)')
    parser.add_argument('--gpu', default=None, type=int, help='GPU id to use.')
    parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                        metavar='LR', help='initial learning rate', dest='lr')
    parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                        help='momentum')
    parser.add_argument('--wd', '--weight-decay', default=1e-4, type=float,
                        metavar='W', help='weight decay (default: 1e-4)',
                        dest='weight_decay')
    parser.add_argument('--resume', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')

    return parser

def parseArgs():
    parser = argparse.ArgumentParser(description='PyTorch ImageNet Training',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('data', metavar='DIR',
                        help='path to dataset')
    parser.add_argument('-l', '--loss', default='crossentropy', choices=['crossentropy', 'focalloss'],
                        help='Loss function')
    parser.add_argument('--epochs', default=90, type=int, metavar='N',
                        help='number of total epochs to run')
    parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                        help='manual epoch number (useful on restarts)')
    parser.add_argument('-p', '--print-freq', default=10, type=int,
                        metavar='N', help='print frequency (default: 10)')
    parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                        help='evaluate model on validation set')
    parser.add_argument('--world-size', default=-1, type=int, help='number of nodes for distributed training')
    parser.add_argument('--rank', default=-1, type=int, help='node rank for distributed training')
    parser.add_argument('--dist-url', default='tcp://224.66.41.62:23456', type=str,
                        help='url used to set up distributed training')
    parser.add_argument('--dist-backend', default='nccl', type=str, help='distributed backend')
    parser.add_argument('--seed', default=None, type=int, help='seed for initializing training. ')
    parser.add_argument('--multiprocessing-distributed', action='store_true',
                        help='Use multi-processing distributed training to launch '
                             'N processes per node, which has N GPUs. This is the '
                             'fastest way to use PyTorch for either single node or '
                             'multi node data parallel training')
    # 2.0 was default in the paper: https://arxiv.org/pdf/1708.02002.pdf
    parser.add_argument('--focalLossGamma', default=2.0, type=float, help='focal loss gamma')

    parser.add_argument('--plot', action='store_true', help='Plot some stuff during training')
    parser.add_argument('--randomResizedCrop', action='store_true', help='If true then scale before cropping')

    parser = addParametersDataLoader(parser)
    parser = addParametersModelLoader(parser)

    parser.add_argument('--savePath', default='./', type=str, help='path to write model checkpoints')
    parser.add_argument('--interactive', action='store_true', help='Keep plots at end')
    return parser.parse_args()


# Global var, needs protection
# TODO add mutex
best_acc1 = 0


def main():
    plt.ion()
    args = parseArgs()

    if not os.path.isdir(args.savePath):
        print('Creating output directory {}'.format(args.savePath))
        os.mkdir(args.savePath)

    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. '
                      'This will turn on the CUDNN deterministic setting, '
                      'which can slow down your training considerably! '
                      'You may see unexpected behavior when restarting '
                      'from checkpoints.')

    if args.gpu is not None:
        warnings.warn('You have chosen a specific GPU. This will completely '
                      'disable data parallelism.')

    if args.dist_url == "env://" and args.world_size == -1:
        args.world_size = int(os.environ["WORLD_SIZE"])

    args.distributed = args.world_size > 1 or args.multiprocessing_distributed

    ngpus_per_node = torch.cuda.device_count()
    if args.multiprocessing_distributed:
        # Since we have ngpus_per_node processes per node, the total world_size
        # needs to be adjusted accordingly
        args.world_size = ngpus_per_node * args.world_size
        # Use torch.multiprocessing.spawn to launch distributed processes: the
        # main_worker process function
        mp.spawn(main_worker, nprocs=ngpus_per_node, args=(ngpus_per_node, args))
    else:
        # Simply call main_worker function
        main_worker(args.gpu, ngpus_per_node, args)

    if args.interactive:
        plt.ioff()
        plt.show()

def getClassCounts(traindir, classNames, fileExts=['jpg', 'png']):
    # It seems there is no simple way in pytorch to get the number of training examples per class.  It's
    # faster just to list files in the folders.
    classCounts = []
    for cname in classNames:
        c = 0
        for ext in fileExts:
            c += len(glob.glob('%s/%s/*.%s' % (traindir, cname, ext)))
        classCounts.append(c)
    return classCounts

def showAugmentations(dataset, n=6, nimg=10):
    #inds = range(min(nimg, len(dataset)))
    inds = np.random.choice(len(dataset), nimg)
    print('inds = {}'.format(inds))
    img = np.vstack((np.hstack((np.asarray(dataset[i][0]).transpose((1,2,0))*CHANNEL_STD + CHANNEL_MEAN \
                                for _ in range(n)))
                     for i in inds))
    plt.imshow(img)
    plt.title('augmentations')
    plt.axis('off')

def getClassCountsOfDataSet(ds):
    classes = ds.classes
    C = len(classes)
    print('Getting training labels...')
    train_labels = [z[1] for z in ds.imgs]
    print('   - done')
    # It's not entirely obvious from the doc, but these are weightings _per data sample_, not per class.
    classCounts = np.bincount(train_labels, minlength=C)
    return classCounts, train_labels

def loadDataSetsArgs( args,
                  pathAttrName='data',
                  distributed=False,
                  plot=False,
                  imgSz=256,
                  ccropSz=224,
                  randomResizedCrop=False ):

    dpath = getattr(args, pathAttrName)
    imgaug, imgaugClouds, weightedSampling, batch_size, workers = args.imgaug, args.imgaugClouds, args.weightedSampling, \
                                                                  args.batch_size, args.workers

    return loadDataSets(dpath, imgaug, imgaugClouds, weightedSampling, batch_size, workers,
                        distributed,
                        plot,
                        imgSz,
                        ccropSz,
                        randomResizedCrop)

def basicTransforms(imgSz, ccropSz):
    normalize = transforms.Normalize(mean=CHANNEL_MEAN, std=CHANNEL_STD)
    valTxforms = transforms.Compose([ transforms.Resize(imgSz),
                                      transforms.CenterCrop(ccropSz),
                                      transforms.ToTensor(),
                                      normalize,
    ])
    return valTxforms

def loadDataSets( dpath,
                  imgaug=False,
                  imgaugClouds=False,
                  weightedSampling=False,
                  batch_size=256,
                  workers=4,
                  distributed=False,
                  plot=False,
                  imgSz=256,
                  ccropSz=224,
                  randomResizedCrop=False,
                  pinMemory=True):
    # Data loading code
    traindir = os.path.join(dpath, 'train')
    print('traindir = {}'.format(traindir))
    valdir = os.path.join(dpath, 'val')
    normalize = transforms.Normalize(mean=CHANNEL_MEAN, std=CHANNEL_STD)

    # Class to be able to apply imgaug.
    class ImgAugTransform:
      def __init__(self, doClouds):
          augList = [
              #iaa.Scale((224, 224)),
              iaa.Sometimes(0.25, iaa.GaussianBlur(sigma=(0, 3.0))),
              #iaa.Affine(rotate=(-20, 20), mode='symmetric'),
              iaa.Rot90([0,1,2,3]),
              iaa.Sometimes(0.25,
                            iaa.OneOf([iaa.Dropout(p=(0, 0.1)),
                                       iaa.CoarseDropout(0.1, size_percent=0.5)])),
              iaa.AddToHueAndSaturation(value=(-10, 10), per_channel=True),
          ]
          if doClouds:
              augList.append(iaa.Clouds())
          self.aug = iaa.Sequential(augList)

      def __call__(self, img):
          # From PIL
          img = np.array(img)
          z = self.aug.augment_image(img)
          return Image.fromarray(z)

    if randomResizedCrop and imgaug:
        #tl = [transforms.RandomResizedCrop(ccropSz)]
        # Want it centred
        tl = [transforms.Resize(imgSz),
              transforms.RandomApply([
                  transforms.RandomAffine(0, scale=(1.0, 2.0), resample=Image.BILINEAR),
                  transforms.CenterCrop(imgSz),
                  ], p=0.3),
              transforms.RandomCrop(ccropSz)]
    else:
        tl = [transforms.Resize(imgSz), transforms.RandomCrop(ccropSz)]

    if imgaug:
        tl.append( ImgAugTransform(imgaugClouds) )

        tl += [
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            ]

    tl += [
        transforms.ToTensor(),
        normalize,
    ]
    trainTxforms = transforms.Compose(tl)
    print(trainTxforms.transforms)
    train_dataset = datasets.ImageFolder( traindir, trainTxforms )
    classes = train_dataset.classes
    C = len(classes)

    if plot:
        plt.figure()
        showAugmentations(train_dataset)
        plt.waitforbuttonpress(0.001)

    # Compute proportion (weight) of  each class.
    # Need the training set targets.
    classCounts, train_labels = getClassCountsOfDataSet(train_dataset)
    assert np.all(classCounts > 0)
    classWeights = 1.0 / classCounts.astype(float)
    print('Class weights:')
    for cname, wt in zip(train_dataset.classes, classWeights):
        print('\t%.8f: %s' % (wt, cname))

    if distributed:
        print('Using distributed sampler')
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    elif weightedSampling:
        print('Using weighted random sampling')
        sampleWeights = classWeights[train_labels]
        train_sampler = torch.utils.data.sampler.WeightedRandomSampler( sampleWeights,
                                                                        len(train_labels),
                                                                        replacement=True )
    else:
        train_sampler = None

    print('Loading training data set   {}'.format(traindir))
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=(train_sampler is None),
        num_workers=workers,
        pin_memory=pinMemory,
        sampler=train_sampler
    )

    print('Loading validation data set {}'.format(valdir))
    valTxforms = basicTransforms(imgSz, ccropSz)
    val_dataset = datasets.ImageFolder(valdir, valTxforms)
    val_loader = torch.utils.data.DataLoader(
        val_dataset, batch_size=batch_size, shuffle=False, num_workers=workers, pin_memory=pinMemory,
    )
    # Use a balanced loader so we always have good class balance for the partial test set error reporting.
    val_labels = [z[1] for z in val_dataset.imgs]
    sampleWeights = classWeights[val_labels]
    val_sampler = torch.utils.data.sampler.WeightedRandomSampler( sampleWeights, len( val_labels ), replacement=True )
    val_loader_balanced = torch.utils.data.DataLoader(
        val_dataset,
        batch_size=batch_size,
        num_workers=workers,
        pin_memory=pinMemory,
        sampler=val_sampler
    )

    print('Train classes:      {}'.format(train_dataset.classes))
    print('Validation classes: {}'.format(val_loader.dataset.classes))

    if plot:
        plt.figure()
        showDataSet(train_loader)
        plt.draw()
        plt.waitforbuttonpress(0.001)

    return train_loader, val_loader, val_loader_balanced


def createModel(pretrained, arch, nbClasses):
    if pretrained:
        print("=> using pre-trained model '{}'".format(arch))
        model = models.__dict__[arch](pretrained=True)
    else:
        print("=> creating model '{}'".format(arch))
        model = models.__dict__[arch]()

    # Replace last layer with new layer for new number of classes.
    # Get the last layer, work out number of features, and replace it with a new linear layer.
    cl = model.classifier
    lastLayerIdx = list(cl._modules.keys())[-1]
    lastLayer = cl._modules[lastLayerIdx]
    inSize = lastLayer.in_features
    print('insize = {}, nbClasses = {}'.format(type(inSize), type(nbClasses)))
    model.classifier._modules[lastLayerIdx] = nn.Linear(inSize, nbClasses, bias=True)
    return model

def loadModelArgs(args, C):

    pretrained, arch, distributed, gpu, batch_size, workers, lr, resume, momentum, weight_decay = args.pretrained, args.arch, \
            args.distributed, args.gpu, args.batch_size, args.workers, args.lr, args.resume, args.momentum, args.weight_decay

    return loadModel(C, pretrained, arch, distributed, gpu, batch_size, workers, lr, resume, momentum, weight_decay)

def loadModel( C,
               pretrained=False,
               arch='vgg16',
               distributed=False,
               gpu=None,
               batch_size=256,
               workers=4,
               lr=0.1,
               resume=False,
               momentum=0.9,
               weight_decay=1E-4 ):
    # create model
    model = createModel(pretrained, arch, C)
    print('loadModel: using gpu {}'.format(gpu))

    if distributed:
        # For multiprocessing distributed, DistributedDataParallel constructor
        # should always set the single device scope, otherwise,
        # DistributedDataParallel will use all available devices.
        if gpu is not None:
            torch.cuda.set_device(gpu)
            model.cuda(gpu)
            # When using a single GPU per process and per
            # DistributedDataParallel, we need to divide the batch size
            # ourselves based on the total number of GPUs we have
            batch_size = int(batch_size / ngpus_per_node)
            workers = int(workers / ngpus_per_node)
            model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[gpu])
        else:
            model.cuda()
            # DistributedDataParallel will divide and allocate batch_size to all
            # available GPUs if device_ids are not set
            model = torch.nn.parallel.DistributedDataParallel(model)
    elif gpu is not None:
        torch.cuda.set_device(gpu)
        model = model.cuda(gpu)
    else:
        # DataParallel will divide and allocate batch_size to all available GPUs
        if arch.startswith('alexnet') or arch.startswith('vgg'):
            model.features = torch.nn.DataParallel(model.features)
            model.cuda()
        else:
            model = torch.nn.DataParallel(model).cuda()

    print('Loaded model = ')
    summary(model, (3, 224, 224))

    optimizer = torch.optim.SGD(model.parameters(), lr,
                                momentum=momentum,
                                weight_decay=weight_decay)

    # optionally resume from a checkpoint
    if resume:
        if os.path.isfile(resume):
            print("=> loading checkpoint '{}'".format(resume))
            checkpoint = torch.load(resume)
            start_epoch = checkpoint['epoch']
            best_acc1 = checkpoint['best_acc1']
            if gpu is not None:
                # best_acc1 may be from a checkpoint from a different GPU
                best_acc1 = best_acc1.to(gpu)
            model.load_state_dict(checkpoint['state_dict'])
            if optimizer is not None:
                optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(resume, checkpoint['epoch']))
        else:
            s = "=> no checkpoint found at '{}'".format(resume)
            print(s)
            # Don't fail silently!!
            raise IOError(s)
    return model, optimizer

def main_worker(gpu, ngpus_per_node, args):
    global best_acc1
    args.gpu = gpu

    if args.gpu is not None:
        print("Use GPU: {} for training".format(args.gpu))

    if args.distributed:
        if args.dist_url == "env://" and args.rank == -1:
            args.rank = int(os.environ["RANK"])
        if args.multiprocessing_distributed:
            # For multiprocessing distributed training, rank needs to be the
            # global rank among all the processes
            args.rank = args.rank * ngpus_per_node + gpu
        dist.init_process_group(backend=args.dist_backend, init_method=args.dist_url,
                                world_size=args.world_size, rank=args.rank)

    # Load the data sets
    train_loader, val_loader, val_loader_balanced = loadDataSetsArgs( args,
                                                                  pathAttrName='data',
                                                                  distributed=args.distributed,
                                                                  plot=args.plot,
                                                                  randomResizedCrop=args.randomResizedCrop)

    classes = train_loader.dataset.classes
    C = len(classes)
    print('Data set has {} classes: {}'.format(C, classes))

    model, optimizer = loadModelArgs(args, C)

    # define loss function (criterion) and optimizer
    if args.loss == 'crossentropy':
        criterion = nn.CrossEntropyLoss().cuda(args.gpu)
    elif args.loss == 'focalloss':
        criterion = focalloss.FocalLoss(gamma=args.focalLossGamma).cuda(args.gpu)
    else:
        print('Unrecognised loss function {}'.format(args.loss))
        sys.exit(1)


    cudnn.benchmark = True

    if args.evaluate:
        validate(val_loader, model, criterion, args.gpu, args.print_freq)
        return

    if args.plot:
        fig, ax = plt.subplots(2,1)
    else:
        fig, ax = None, None

    for epoch in range(args.start_epoch, args.epochs):
        if args.distributed:
            train_sampler.set_epoch(epoch)
        adjust_learning_rate(optimizer, epoch, args.lr)

        # train for one epoch
        train(
            train_loader,
            val_loader_balanced,
            model,
            criterion,
            optimizer,
            epoch,
            args.gpu,
            args.print_freq,
            ax
        )

        # evaluate on validation set
        acc1, acc5, accPC, lossVal, _ = validate(val_loader, model, criterion, args.gpu, args.print_freq)
        acc1 = acc1.cpu()
        if args.plot:
            xval = (1+epoch)*len(train_loader)-1
            ax[0].plot(xval, lossVal, 'ro')
            ax[1].plot(xval, accPC, 'ro')

        # remember best acc@1 and save checkpoint
        is_best = acc1 > best_acc1
        best_acc1 = max(acc1, best_acc1)

        if not args.multiprocessing_distributed or (args.multiprocessing_distributed
                and args.rank % ngpus_per_node == 0):
            save_checkpoint({
                'epoch': epoch + 1,
                'arch': args.arch,
                'state_dict': model.state_dict(),
                'best_acc1': best_acc1,
                'optimizer' : optimizer.state_dict(),
            }, is_best, args.savePath, 'checkpoint_%08d' % epoch)


def imshowTensor(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    inp = CHANNEL_STD * inp + CHANNEL_MEAN
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)  # pause a bit so that plots are updated


def showDataSet(trainLoader):
    # inspired by https://pytorch.org/tutorials/beginner/transfer_learning_tutorial.html
    class_names = trainLoader.dataset.classes
    # Get a batch of training data
    inputs, classes = next(iter(trainLoader))
    # Make a grid from batch
    out = torchvision.utils.make_grid(inputs)
    imshowTensor(out, title=[class_names[x] for x in classes])
    plt.waitforbuttonpress(0.0001)

def train(train_loader, val_loader, model, criterion, optimizer, epoch, gpu, print_freq, ax=None):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()
    C = len(train_loader.dataset.classes)
    perClass = AverageMeterPerClass(C)

    # switch to train mode
    model.train()

    end = time.time()
    #allLosses, allAccs = [], []
    plotStart = epoch * len(train_loader)

    valBatchIter=None
    valNbBatches=5

    for i, (input, target) in enumerate(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        #print('batch target = {}'.format(target))

        if gpu is not None:
            input = input.cuda(gpu, non_blocking=True)
            target = target.cuda(gpu, non_blocking=True)

        # compute output
        output = model(input)
        #print('output has type  {} and shape {} and is {}'.format(type(output), output.shape, output))
        loss = criterion(output, target)

        # measure accuracy and record loss
        acc1, acc5, confusion = accuracy(C, output, target, topk=(1, min(C,5)))
        losses.update(loss.item(), input.size(0))
        top1.update(acc1[0], input.size(0))
        top5.update(acc5[0], input.size(0))
        perClass.update(confusion)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        #allLosses.append(losses.val)
        #allAccs.append(np.nanmean(perClass.avg))

        if i % print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Acc@1 {top1.val:7.3f} ({top1.avg:7.3f})\t'
                  'Acc@5 {top5.val:7.3f} ({top5.avg:7.3f})\t'
                  'AccPerClass {acpVal:7.3f} ({acpAvg})'.format(
                   epoch, i, len(train_loader), batch_time=batch_time,
                      data_time=data_time, loss=losses, top1=top1, top5=top5,
                      acpVal=np.nanmean(perClass.val), acpAvg=np.nanmean(perClass.avg)))

            if ax is not None:
                # Show loss on axes.
                axLoss, axAcc = ax
                #axLoss.cla()
                #xvals = plotStart+np.arange(len(allLosses))
                xval = plotStart+i
                axLoss.plot(xval, losses.val, 'b.')
                axLoss.set_title('Loss: epoch %3d, iter %8d' % (epoch, i))
                axLoss.grid(1)

                #axAcc.cla()
                axAcc.plot(xval, np.nanmean(perClass.avg), 'b.')
                axAcc.set_title('Average per-class accuracy: epoch %3d, iter %8d' % (epoch, i))
                axAcc.grid(1)

                if i % (print_freq*4) == 0:
                    # partially evaluate on validation set
                    acc1, acc5, accPC, lossVal, valBatchIter = validate(
                        val_loader,
                        model,
                        criterion,
                        gpu,
                        print_freq,
                        valBatchIter,
                        valNbBatches,
                        silent=True
                    )
                    acc1 = acc1.cpu()
                    ax[0].plot(xval, lossVal, 'r.')
                    ax[1].plot(xval, accPC, 'r.')

                    model.train()

                plt.waitforbuttonpress(0.0000001)

    print('Training complete.')

def validate(
        val_loader,
        model,
        criterion,
        gpu,
        print_freq,
        batch_iter=None,
        nbBatches=None,
        silent=False,
        batchSilent=False
):
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()
    C = len(val_loader.dataset.classes)
    perClass = AverageMeterPerClass(C)
    data_time = AverageMeter()

    # switch to evaluate mode
    model.eval()

    if nbBatches is None or nbBatches > len(val_loader):
        nbBatches = len(val_loader)

    with torch.no_grad():
        end = time.time()
        #for i, (input, target) in enumerate(val_loader):
        if batch_iter == None:
            batch_iter = iter(val_loader)
        for i in range(nbBatches):
            try:
                input, target = next(batch_iter)
            except StopIteration as e:
                print('WARNING in validate: ran out of data early')
                # This forces reset of iterator
                batch_iter = None
                break

            if gpu is not None:
                input = input.cuda(gpu, non_blocking=True)
                target = target.cuda(gpu, non_blocking=True)
            data_time.update(time.time() - end)

            #print('batch {} of {}: classes = {}'.format(i, nbBatches, target))

            # compute output
            output = model(input)
            loss = criterion(output, target)

            #print('output = {}'.format(output.argmax(1)))

            # measure accuracy and record loss
            acc1, acc5, confusion = accuracy(C, output, target, topk=(1, min(C,5)))
            losses.update(loss.item(), input.size(0))
            top1.update(acc1[0], input.size(0))
            top5.update(acc5[0], input.size(0))
            perClass.update(confusion)

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if not batchSilent and  i % print_freq == 0:
                print('Test: [{0}/{1}]\t'
                      'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                      'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                      'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                      'Acc@1 {top1.val:7.3f} ({top1.avg:7.3f})\t'
                      'Acc@5 {top5.val:7.3f} ({top5.avg:7.3f})\t'
                      'AccPerClass {acpVal:7.3f} ({acpAvg})'.format(
                       i, len(val_loader), batch_time=batch_time, data_time=data_time, loss=losses,
                       top1=top1, top5=top5,
                          acpVal=np.nanmean(perClass.val), acpAvg=np.nanmean(perClass.avg)))

        if not silent:
            print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f} AccPerClass {acpAvg:.3f}'
                  .format(top1=top1, top5=top5, acpAvg=np.nanmean(perClass.avg)))
            print('Per class accuracy:')
            for cls, acc in zip(val_loader.dataset.classes, perClass.avg):
                print('\t{classname:35s}: {acc:7.3f}%'.format(classname=cls, acc=acc))
    return top1.avg, top5.avg, np.nanmean(perClass.avg), losses.val, batch_iter


def save_checkpoint(state, is_best, savePath='.', filenameBase='checkpoint', filenameBestBase='model_best'):
    ofn = '%s/%s.pth.tar' % (savePath, filenameBase)
    torch.save(state, ofn)
    if is_best:
        ofnBest = '%s/%s.pth.tar' % (savePath, filenameBestBase)
        shutil.copyfile(ofn, ofnBest)


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def confusionToAccuracy(confusion):
    print(confusion)
    # Correct count per class
    numer = np.diag(confusion).astype(float)
    assert np.all(np.isfinite(confusion))
    # Total count per class
    denom = np.sum(confusion, axis=1).astype(float)
    acc = 100.0 * numer / denom
    return acc

def nansum(a,b):
    assert len(a) == len(b)
    # Just want to make sure if one of these is nan it gets treated like zero.
    aa = a.copy()
    aa[np.isnan(a)] = 0
    bb = b.copy()
    bb[np.isnan(b)] = 0
    return aa + bb

class AverageMeterVec(object):
    """Computes and stores the average and current value"""
    def __init__(self, N):
        self.N = N
        self.reset()

    def reset(self):
        self.val = np.zeros((self.N,))
        self.avg = np.zeros((self.N,))
        self.sum = np.zeros((self.N,))
        self.count = 0

    def update(self, val, n=1):
        assert len(val) == len(self.val)
        self.val[:] = val
        self.sum = nansum(self.sum, val * n)
        self.count += n
        self.avg = self.sum / self.count


class AverageMeterPerClass(object):
    """Maintains a running accuracy per class"""
    def __init__(self, N):
        self.N = N
        self.reset()

    def reset(self):
        self.classCount = np.zeros((self.N,))
        self.correctCount = np.zeros((self.N,))
        # Current per class accuracy
        self.val = np.zeros((self.N,))
        # Average per class accuracy
        self.avg = np.zeros((self.N,))

    def update(self, confusion):
        #print('Updating with confusion = \n{}'.format(confusion))
        assert confusion.shape == (self.N, self.N)
        self.classCount += confusion.sum(axis=1)
        self.correctCount += np.diag(confusion)
        numer = np.diag(confusion).astype(float)
        denom =  np.sum(confusion, axis=1).astype(float)
        self.val[:] = numer/np.maximum(1.0,denom)*100.0
        self.avg[:] = self.correctCount / np.maximum(1.0, self.classCount) * 100.0
        #print('Instantaneous acc is {}'.format(self.val))
        #print('Running average is {}'.format(self.avg))

def adjust_learning_rate(optimizer, epoch, lr):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = lr * (0.1 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
    return lr

def accuracy(C, output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k.
       C is number of classes."""
    confusion = np.zeros((C,C), dtype=int)

    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        #print('output={}, maxk={}'.format(output.shape, maxk))
        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        #print('pred = {}'.format(pred.shape))
        # Target is a vector (tensor)
        correct = pred.eq(target.view(1, -1).expand_as(pred))
        # this tensor is k x batchSize and it's on gpu

        # Compute per-class accuracy

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))

        #print('correct has shape {} and = {}'.format(correct.shape, correct))
        #print('target has shape {} and = {}'.format(target.shape, target))
        #print('pred has shape {} and = {}'.format(pred.shape, pred))
        #print('res = {}'.format(res))

        for i in range(batch_size):
            confusion[target[i], pred[0, i]] += 1

        res.append(confusion)
        #print(confusion)
        return res


if __name__ == '__main__':
    main()
